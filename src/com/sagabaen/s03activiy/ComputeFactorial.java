package com.sagabaen.s03activiy;

import java.util.Scanner;

public class ComputeFactorial {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        var num = 0; // int
        System.out.println("Enter number whose factorial shall be computed: ");
        try {
            num = input.nextInt();
            long result = 1;
            for (var i = num; i > 0; i-- ) {
                result = result * i;
            }
            if (num < 0) {
                 throw new Exception();
            }
            if (result < 0) {
                System.out.println(-result);
            } else {
                System.out.println(result);
            }

        } catch (Exception err) {
            if(num < 0){
                System.out.println("The number you provided was negative!");
            } else {
                System.out.println("You did not input a number!");
            }

        }

    }
}
